package com.afs.restapi;

import com.afs.restapi.dto.Employee;
import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.repository.EmployeeRepository;
import com.afs.restapi.service.EmployeeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

    @Mock
    EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_less_than_18_or_greater_than_65() {
        //given
        Employee newEmployee = new Employee("Tom", 16, "Male", 1000);

        EmployeeService employeeService = new EmployeeService(employeeRepository);

        //given
        Throwable exception = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(newEmployee);
        });


        assertEquals("Employee must be 18~65 years old", exception.getMessage());
    }

    @Test
    void should_throw_invalid_employee_when_create_given_employee_age_greater_than_65() {
        //given
        Employee newEmployee = new Employee("Tom", 16, "Male", 1000);

        EmployeeService employeeService = new EmployeeService(employeeRepository);

        //given
        Throwable exception = assertThrows(InvalidEmployeeException.class, () -> {
            employeeService.create(newEmployee);
        });

        assertEquals("Employee must be 18~65 years old", exception.getMessage());
    }

    @Test
    void should_add_new_employee_when_create_given_employee_age_in_range() {
        //given
        Employee newEmployee = new Employee("Tom", 45, "Male", 50000);

        employeeService.create(newEmployee);

        verify(employeeRepository, times(1)).insert(newEmployee);
    }

    @Test
    void should_return_all_employees_when_find_all() {
        employeeService.findAll();

        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_return_target_employee_when_find_employee_by_id_given_id() {
        Long id = 1L;

        employeeService.findById(id);

        verify(employeeRepository, times(1)).findEmployeeById(id);
    }

    @Test
    void should_return_employee_when_find_employee_by_gender_given_gender() {
        String gender = "male";

        employeeService.findByGender(gender);

        verify(employeeRepository, times(1)).findEmployeeByGender(gender);
    }

    @Test
    void should_return_employee_by_page_when_find_by_page_given_page_number_and_page_size() {
        Integer pageNumber = 1;
        Integer pageSize = 10;

        employeeService.findByPage(pageNumber, pageSize);

        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_delete_employee_when_delete_by_id_given_id() {
        Long id = 1L;

        employeeService.deleteById(id);

        verify(employeeRepository, times(1)).deleteById(id);
    }

    @Test
    void should_throw_InactiveEmployeeException_when_update_employee_given_inactive_employee() {
        Long id = 1L;
        Employee targetEmployee = new Employee(1L, "Lucy", 20, "female", 8000);
        targetEmployee.setIsActive(false);
        Employee employeeTobeUpdated = new Employee(1L, "Lucy", 20, "female", 8000);

        when(employeeRepository.findEmployeeById(id)).thenReturn(targetEmployee);

        Throwable exception = assertThrows(InactiveEmployeeException.class, () -> employeeService.updateEmployee(employeeTobeUpdated, id));

        assertEquals("Employee is inactive", exception.getMessage());
    }

    @Test
    void should_throw_InvalidEmployeeException_when_update_employee_given_employee_without_salary_and_age() {
        Long id = 1L;
        Employee employeeTobeUpdated = new Employee(1L, null, 20, "female", null);
        Employee targetEmployee = new Employee(1L, "Lucy", 20, "female", 8000);
        targetEmployee.setIsActive(true);

        when(employeeRepository.findEmployeeById(id)).thenReturn(targetEmployee);

        Throwable exception = assertThrows(InvalidEmployeeException.class, () -> employeeService.updateEmployee(employeeTobeUpdated, id));

        assertEquals("Age and salary cannot be null", exception.getMessage());
    }
}
