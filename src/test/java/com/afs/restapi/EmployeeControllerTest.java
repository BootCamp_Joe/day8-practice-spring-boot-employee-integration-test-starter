package com.afs.restapi;

import com.afs.restapi.dto.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    MockMvc postmanMock;
    ObjectMapper objectMapper = new ObjectMapper();
    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setup(){
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_perform_get_given_employee() throws Exception {
        // given
        // data resource has x employees

        //when
        //http client send GET "/employees" => postman send GET and request
        //test double => fake, mock, stub, spy ... => postman test double send GET "/employees" and request
        postmanMock.perform(MockMvcRequestBuilders.get("/employees"))

        //then
        //response should return x employees
        // test double => postman test double response and assert
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(4)));
    }

    @Test
    void should_return_target_employee_when_perform_get_given_employee_id() throws Exception {
        // given
        //when
        postmanMock.perform(MockMvcRequestBuilders.get("/employees/1"))
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(8000));

    }

    @Test
    void should_return_204_when_perform_delete_given_employee_id() throws Exception {
        // given
        //when
        postmanMock.perform(MockMvcRequestBuilders.delete("/employees/1"))
                //then
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Employee removedEmployee = employeeRepository.findEmployeeById(1L);
        assertFalse(removedEmployee.getIsActive());
    }

    @Test
    void should_return_male_employee_when_perform_get_given_employee_gender() throws Exception {
        // given
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("gender", "female");
        //when
        postmanMock.perform(requestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(3)))
                .andExpect(MockMvcResultMatchers.jsonPath("$..gender").value(everyItem(is("female"))))
        ;
    }

    @Test
    void should_return_employee_when_perform_get_given_page_number_and_size() throws Exception {
        // given
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get("/employees")
                .param("pageNumber", "2")
                .param("pageSize", "2");

        List<Employee> targetEmployee = new ArrayList<>();
        targetEmployee.add(new Employee(3L, "Lily", 22, "female", 2800, true));
        targetEmployee.add(new Employee(4L, "Alice", 25, "female", 3800, true));
        //when
        postmanMock.perform(requestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(2)))
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(targetEmployee)))
        ;
    }

    @Test
    void should_return_new_employee_when_perform_post_given_new_employee() throws Exception {
        // given
        Employee newEmployee = new Employee(null, "Lulu", 23, "female", 4000);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.post("/employees")
                .content(objectMapper.writeValueAsString(newEmployee))
                .contentType(MediaType.APPLICATION_JSON);


        //when
        postmanMock.perform(requestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lulu"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(23))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(4000))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.isActive").value(true));


        List<Employee> employeesAfterDeletion = employeeRepository.findAll();
        assertEquals(5, employeesAfterDeletion.size());

    }

    @Test
    void should_return_updated_employee_when_perform_put_given_employee_and_id() throws Exception {
        // given
        Employee updatedEmployee = new Employee(1L, "Lucy", 21, "female", 4000);
        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.put("/employees/1")
                .content(objectMapper.writeValueAsString(updatedEmployee))
                .contentType(MediaType.APPLICATION_JSON);


        //when
        postmanMock.perform(requestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(21))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(4000));

    }

    @Test
    void should_return_204_when_perform_delete_given_id() throws Exception {
        // given
        MockHttpServletRequestBuilder deleteEmployeeRequestBuilder = MockMvcRequestBuilders.delete("/employees/1");

        //when
        postmanMock.perform(deleteEmployeeRequestBuilder);

        List<Employee> employeesAfterDeletion = employeeRepository.findAll();
        assertEquals(3, employeesAfterDeletion.size());

    }

    @Test
    void should_return_404_when_perform_get_given_wrong_employee_id() throws Exception {
        // given
        MockHttpServletRequestBuilder getEmployeeRequestBuilder = MockMvcRequestBuilders.get("/employees/888");
        //when
        postmanMock.perform(getEmployeeRequestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @Test
    void should_return_404_when_perform_update_given_wrong_employee_id() throws Exception {

        Employee targetEmployee = new Employee(1L, "Lucy", 21, "female", 4000);

        // given
        MockHttpServletRequestBuilder putEmployeeRequestBuilder = MockMvcRequestBuilders.put("/employees/888")
                                                                                        .content(objectMapper.writeValueAsString(targetEmployee))
                                                                                        .contentType(MediaType.APPLICATION_JSON);;

        //when
        postmanMock.perform(putEmployeeRequestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @Test
    void should_return_404_when_perform_delete_given_wrong_employee_id() throws Exception {

        MockHttpServletRequestBuilder deleteEmployeeRequestBuilder = MockMvcRequestBuilders.delete("/employees/888");

        //when
        postmanMock.perform(deleteEmployeeRequestBuilder)
                //then
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }
}
