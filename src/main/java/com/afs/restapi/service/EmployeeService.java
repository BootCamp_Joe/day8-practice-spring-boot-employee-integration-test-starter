package com.afs.restapi.service;

import com.afs.restapi.dto.Employee;
import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class EmployeeService {

    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee newEmployee) {
        if (newEmployee.getAge() < 18 || newEmployee.getAge() > 65) throw new InvalidEmployeeException("Employee must be 18~65 years old");
        newEmployee.setIsActive(true);
        return employeeRepository.insert(newEmployee);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findEmployeeById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findEmployeeByGender(gender);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }


    public Employee updateEmployee(Employee employee, Long id) {
        Employee targetEmployee = employeeRepository.findEmployeeById(id);
        if(Boolean.FALSE.equals(targetEmployee.getIsActive())) {
            throw new InactiveEmployeeException("Employee is inactive");
        }else if(Objects.isNull(employee.getAge()) || Objects.isNull(employee.getSalary())) {
            throw new InvalidEmployeeException("Age and salary cannot be null");
        }
        return employeeRepository.updateEmployee(targetEmployee, employee);
    }

    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
