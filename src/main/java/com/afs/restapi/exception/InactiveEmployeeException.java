package com.afs.restapi.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InactiveEmployeeException extends RuntimeException{
    public InactiveEmployeeException(String message) {
        super(message);
    }
}
