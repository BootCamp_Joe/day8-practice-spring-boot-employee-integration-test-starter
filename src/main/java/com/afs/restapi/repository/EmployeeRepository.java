package com.afs.restapi.repository;

import com.afs.restapi.dto.Employee;
import com.afs.restapi.exception.NotFoundException;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees;

    public EmployeeRepository(){
        this.employees = new ArrayList<>();
        init();
    }

    public void reset(){
        employees.clear();
        init();
    }

    public void init() {
        employees.add(new Employee(1L, "Lucy", 20, "female", 8000, true));
        employees.add(new Employee(2L, "Ben", 30, "male", 2000, true));
        employees.add(new Employee(3L, "Lily", 22, "female", 2800, true));
        employees.add(new Employee(4L, "Alice", 25, "female", 3800, true));
    }

    public List<Employee> findAll() {
        return employees.stream()
                .filter(Employee::getIsActive)
                .collect(Collectors.toList());
    }

    public Employee findEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .orElseThrow(() -> new NotFoundException("Not Found"));
    }

    public List<Employee> findEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee insert(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    private Long generateId() {
        return employees.stream()
                .mapToLong(Employee::getId)
                .max()
                .orElse(0L)
                + 1;
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Employee updateEmployee(Employee targetEmployee, Employee employee) {
        targetEmployee.merge(employee);
        return targetEmployee;
    }

    public void deleteById(Long id) {
        Employee targetEmployee = findEmployeeById(id);
        targetEmployee.setIsActive(false);
    }

}
